package com.bark.DecoratorPattern;

import com.bark.DecoratorPattern.coffee.Beverage;
import com.bark.DecoratorPattern.coffee.DarkRoast;
import com.bark.DecoratorPattern.coffee.Espresso;
import com.bark.DecoratorPattern.coffee.HouseBlend;
import com.bark.DecoratorPattern.condiment.Mocha;
import com.bark.DecoratorPattern.condiment.Soy;
import com.bark.DecoratorPattern.condiment.Whip;

/**
 * Created by bark on 2017/9/3.
 */
public class Test {
    public static void main(String[] args) {
        Beverage beverage = new Espresso();
        //不要调料的浓缩咖啡
        System.out.println(beverage.getDescription()
                + " $" + beverage.cost());
        //两摩卡+一酱油的深焙
        Beverage beverage2 = new DarkRoast();
        beverage2 = new Mocha(beverage2);
        beverage2 = new Mocha(beverage2);
        beverage2 = new Whip(beverage2);
        System.out.println(beverage2.getDescription()
                + " $" + beverage2.cost());
        //低因咖啡+酱料+摩卡+酱油
        Beverage beverage3 = new HouseBlend();
        beverage3 = new Soy(beverage3);
        beverage3 = new Mocha(beverage3);
        beverage3 = new Whip(beverage3);
        System.out.println(beverage3.getDescription()
                + " $" + beverage3.cost());
        System.out.println(2.33+0.66+0.33+0.20);
    }
}
