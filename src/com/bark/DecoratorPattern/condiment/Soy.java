package com.bark.DecoratorPattern.condiment;

import com.bark.DecoratorPattern.coffee.Beverage;
import com.sun.jndi.ldap.Ber;

/**
 * Created by bark on 2017/9/3.
 */
public class Soy extends CondimentDecorator {
    Beverage beverage;
    public Soy(Beverage beverage) {
        this.beverage = beverage;
    }
    public double cost() {
        return 0.33 + beverage.cost();
    }
    @Override
    public String getDescription() {
        return beverage.getDescription()+",酱油";
    }
}
