package com.bark.DecoratorPattern.condiment;

import com.bark.DecoratorPattern.coffee.Beverage;
import com.sun.jndi.ldap.Ber;

/**
 * Created by bark on 2017/9/3.
 */
public class Whip extends CondimentDecorator {
    private Beverage beverage;
    public  Whip (Beverage bb){
        this.beverage = bb;
    }
    public double cost() {
        return 0.66 + beverage.cost();
    }
    @Override
    public String getDescription() {
        return beverage.getDescription()+",酱料";
    }
}
