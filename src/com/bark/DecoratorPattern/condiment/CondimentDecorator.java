package com.bark.DecoratorPattern.condiment;

import com.bark.DecoratorPattern.coffee.Beverage;

/**
 * Created by bark on 2017/9/3.
 */
public abstract class CondimentDecorator extends Beverage {
    public abstract String getDescription();
    @Override
    public double cost() {
        return 0;
    }
}
