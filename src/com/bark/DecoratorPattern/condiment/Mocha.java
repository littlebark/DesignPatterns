package com.bark.DecoratorPattern.condiment;

import com.bark.DecoratorPattern.coffee.Beverage;

/**
 * Created by bark on 2017/9/3.
 */
public class Mocha extends CondimentDecorator {
    Beverage beverage;
    public Mocha(Beverage beverage) {
        this.beverage = beverage;
    }
    @Override
    public String getDescription() {
        return beverage.getDescription()+",摩卡";
    }

    public double cost() {
        return 0.20 + beverage.cost();
    }
}
