package com.bark.DecoratorPattern.coffee;

/**
 * Created by bark on 2017/9/3.
 */
public class DarkRoast extends Beverage {
    public DarkRoast(){
        description = "深焙";
    }

    @Override
    public double cost() {
        return 0.222;
    }
}
