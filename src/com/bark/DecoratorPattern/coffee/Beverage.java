package com.bark.DecoratorPattern.coffee;

/**
 * Created by bark on 2017/9/3.
 */
public abstract class Beverage {
    String description = "原饮料";

    public String getDescription() {
        return description;
    }
    public abstract double cost();
}
