package com.bark.DecoratorPattern.coffee;

/**
 * Created by bark on 2017/9/3.
 */
public class Espresso extends Beverage {
    public Espresso() {
        description = "浓缩咖啡";
    }
    @Override
    public double cost() {
        return 1.9;
    }
}
