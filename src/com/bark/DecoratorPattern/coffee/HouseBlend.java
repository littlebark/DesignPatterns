package com.bark.DecoratorPattern.coffee;

/**
 * Created by bark on 2017/9/3.
 */
public class HouseBlend extends  Beverage {
    public HouseBlend() {
        description = "低因咖啡";
    }
    @Override
    public double cost() {
        return 2.33;
    }
}
