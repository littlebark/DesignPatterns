package com.bark.StrategyPattern.Role;

import com.bark.StrategyPattern.Weapon.Sword;
import com.bark.StrategyPattern.Weapon.WeaponBehavior;

/**
 * Created by bark on 2017/9/2.
 */
public class Hero {
    private WeaponBehavior weaponBehavior;
    public void Roar(){
        System.out.println("战吼：提升5%攻击力");
    }
    public void quit(){
        System.out.println("逃兵。");
    }
    public Hero(){
        this.weaponBehavior = new Sword();
    }


    public void attackByWeapon(){
        weaponBehavior.attack();
    }
    public void setWeaponBehavior(WeaponBehavior wb){
        weaponBehavior = wb;
    }
}
