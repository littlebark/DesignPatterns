package com.bark.StrategyPattern.Weapon;

/**
 * Created by bark on 2017/9/2.
 */
public class Lance implements WeaponBehavior {
    @Override
    public void attack() {
        System.out.println("长枪突刺！");
    }
}
