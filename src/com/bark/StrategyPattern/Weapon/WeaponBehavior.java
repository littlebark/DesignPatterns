package com.bark.StrategyPattern.Weapon;

/**
 * Created by bark on 2017/9/2.
 */
public interface WeaponBehavior {
    public void attack();
}
