package com.bark.StrategyPattern.Weapon;

/**
 * Created by bark on 2017/9/2.
 */
public class Sword implements WeaponBehavior{
    @Override
    public void attack() {
        System.out.println("剑击！三连刺！");
    }
}
