package com.bark.StrategyPattern;

import com.bark.StrategyPattern.Role.Hero;
import com.bark.StrategyPattern.Role.King;
import com.bark.StrategyPattern.Role.Queen;
import com.bark.StrategyPattern.Weapon.Aex;
/**
 * Created by bark on 2017/9/2.
 */
public class Test {
    public static void main(String[] args) {
        Hero queen = new Queen();
        Hero king = new King();
        queen.Roar();
        king.attackByWeapon();
        king.setWeaponBehavior(new Aex());
        king.attackByWeapon();
    }
}
