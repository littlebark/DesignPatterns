package com.bark.BuilderPattern;

/**
 * Created by bark on 2017/10/14.
 */
public interface DoppelgangerBuilder {
    //创造产品
    public void createDoppelganger(String name);

    public void buildBody();

    public void buildHead();

    public void buildLeftArm();

    public void buildLeftHand();

    public void buildRightArm();

    public void buildRightHand();

    public void buildLeftLeg();

    public void buildLeftFoot();

    public void buildRightLeg();

    public void buildRightFoot();
    //返回产品
    public Doppelganger getDoppelganger();
}
