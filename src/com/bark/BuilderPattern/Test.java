package com.bark.BuilderPattern;

/**
 * Created by bark on 2017/10/14.
 */
public class Test {
    public static void main(String[] args) {
        Soul soul = new Soul();
        //制造一个阿瘦
        System.out.println("获得了" + soul.createDoppelganger(new ThinBuilder(),"阿瘦"));
        System.out.println("----------------------------------------");

        //制造一个阿胖
        System.out.println("获得了" + soul.createDoppelganger(new FatBuilder(),"阿胖"));
    }
}
