package com.bark.ObserverPattern;

/**
 * Created by bark on 2017/9/2.
 */
public interface DisplayElement {
    public void display();
}
