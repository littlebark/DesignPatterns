package com.bark.ObserverPattern.Observer;

import com.bark.ObserverPattern.DisplayElement;
import com.bark.ObserverPattern.Subject.WeatherData;

import java.util.Observable;
import java.util.Observer;

/**
 * Created by bark on 2017/9/2.
 */
public class CurrentConditionsDisplay implements Observer,DisplayElement {
    Observable observable;
    private float temperature;
    private float humidity;

    public CurrentConditionsDisplay(Observable observable) {
        this.observable = observable;
        observable.addObserver(this);
    }
    @Override
    public void display() {
        System.out.println("当前情况: " + temperature
                + "F 度 and " + humidity + "% humidity");
    }
    //这种主题把自身传给观察者通过开放get方法传数据的模式叫拉模式（观察者自己获取数据
    //如果传送的是具体数据而不是主题本身或者封装的数据就是推模式，这俩有类似之处（具体数据的话观察者就没法选择数据了
    @Override
    public void update(Observable o, Object arg) {
        if (o instanceof WeatherData) {
            WeatherData weatherData = (WeatherData)o;
            this.temperature = weatherData.getTemperature();
            this.humidity = weatherData.getHumidity();
            display();
        }
    }
}
