package com.bark.ObserverPattern.Observer;

import com.bark.ObserverPattern.DisplayElement;
import com.bark.ObserverPattern.Subject.WeatherData;

import java.util.Observable;
import java.util.Observer;

/**
 * Created by bark on 2017/9/2.
 */
public class AverageDisplay implements Observer,DisplayElement {
    Observable observable;
    private float temperature;
    private float humidity;

    public AverageDisplay(Observable observable) {
        this.observable = observable;
        observable.addObserver(this);
    }

    @Override
    public void display() {
        System.out.println("平均温度我不会算，加起来吧。它的值是："+this.temperature+this.humidity);
    }

    @Override
    public void update(Observable o, Object arg) {
        if(o instanceof WeatherData){
            WeatherData we =  (WeatherData) o;
            this.temperature = we.getTemperature();
            this.humidity = we.getHumidity();
            display();
        }
    }
}
