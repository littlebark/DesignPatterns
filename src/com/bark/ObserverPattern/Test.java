package com.bark.ObserverPattern;

import com.bark.ObserverPattern.Observer.AverageDisplay;
import com.bark.ObserverPattern.Observer.CurrentConditionsDisplay;
import com.bark.ObserverPattern.Subject.WeatherData;

/**
 * Created by bark on 2017/9/2.
 */
public class Test {
    public static void main(String[] args) {
        WeatherData weatherData = new WeatherData();
        AverageDisplay avg = new AverageDisplay(weatherData);
        CurrentConditionsDisplay currentDisplay = new CurrentConditionsDisplay(weatherData);

        weatherData.setMeasurements(80, 65, 30.4f);
        weatherData.setMeasurements(82, 70, 29.2f);
        weatherData.setMeasurements(78, 90, 29.2f);
    }
}
