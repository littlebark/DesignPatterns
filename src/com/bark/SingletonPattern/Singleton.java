package com.bark.SingletonPattern;

import com.sun.org.apache.xpath.internal.SourceTree;

/**
 * Created by bark on 2017/9/3.
 */
public class Singleton {
    private Singleton() {
    }

    public Singleton getInsance() {
        return SingletonInstance.instance;
    }
    //左潇龙提供的静态内部类的方式
    //静态内部类只有在方法被调用时才会被初始化

    //Singleton最多只有一个实例，在不考虑反射强行突破访问限制的情况下。
    // 保证了并发访问的情况下，不会发生由于并发而产生多个实例。
    //保证了并发访问的情况下，不会由于初始化动作未完全完成而造成使用了尚未正确初始化的实例。
    private static class SingletonInstance {
        static Singleton instance = new Singleton();
    }
}
