package com.bark.SingletonPattern;

/**
 * Created by bark on 2017/9/3.
 */
public class SingletonQuick {
    private static SingletonQuick unique = new SingletonQuick();
    private SingletonQuick(){

    }
    //直接先实例化一个，之后就直接返回，这个小缺点就是一定会实例化，不能做到在用时才实例化
    public static SingletonQuick getInstance(){
        return unique;
    }
}
