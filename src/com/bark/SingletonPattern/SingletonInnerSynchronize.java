package com.bark.SingletonPattern;

/**
 * Created by bark on 2017/9/3.
 */
public class SingletonInnerSynchronize {
    //volatile 确保多线程时正确初始化该变量
    private volatile static SingletonInnerSynchronize unique;
    private  SingletonInnerSynchronize(){

    }
    //双重锁，确保第一次为空时才同步，初始化之后都不会有同步操作了,提高性能
    public static SingletonInnerSynchronize getInstance(){
        if(unique == null){
            synchronized (SingletonInnerSynchronize.class){
                if(unique == null){
                    unique = new SingletonInnerSynchronize();
                }
            }
        }
        return unique;
    }

}
