package com.bark.SingletonPattern;

/**
 * Created by bark on 2017/9/3.
 */
public class SingletonSynchronize {
    private  static  SingletonSynchronize unique;
    private  SingletonSynchronize(){

    }
    //缺点，每次都同步，性能差
    public static synchronized SingletonSynchronize getInstance(){
        if(unique == null){
            unique = new SingletonSynchronize();
        }
        return unique;
    }

}
