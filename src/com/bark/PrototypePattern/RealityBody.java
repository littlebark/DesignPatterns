package com.bark.PrototypePattern;

import java.io.*;

/**
 * Created by littlebark on 2017/9/29.
 */
public class RealityBody implements Serializable {
    private String foot;
    private String hand;
    private Attachment attachment;

    public RealityBody() {
    }

    public RealityBody deepClone() throws Exception{
        //写入当前对象

        // save the object to a byte array
        //将该对象序列化成流,因为写在流里的是对象的一个拷贝，而原对象仍然存在于JVM里面
        ByteArrayOutputStream bao = new ByteArrayOutputStream();
        ObjectOutputStream oo =new ObjectOutputStream(bao);
        oo.writeObject(this);
        oo.close();
        // read a clone of the object from the byte array
        ByteArrayInputStream bai = new ByteArrayInputStream(bao.toByteArray());
        ObjectInputStream in = new ObjectInputStream(bai);
        Object o = in.readObject();
        in.close();
        return (RealityBody) o;
    }
    public RealityBody(String foot, String hand, Attachment attachment) {
        this.foot = foot;
        this.hand = hand;
        this.attachment = attachment;
    }

    public String getFoot() {
        return foot;
    }

    public void setFoot(String foot) {
        this.foot = foot;
    }

    public Attachment getAttachment() {
        return attachment;
    }

    public void setAttachment(Attachment attachment) {
        this.attachment = attachment;
    }

    public String getHand() {
        return hand;
    }

    public void setHand(String hand) {
        this.hand = hand;
    }

    @Override
    public String toString() {
        return "RealityBody{" +
                "foot='" + foot + '\'' +
                ", hand='" + hand + '\'' +
                ", attachment=" + attachment +
                '}';
    }
}
