package com.bark.PrototypePattern;

/**
 * Created by littlebark on 2017/9/28.
 */
public class NormalBark implements Cloneable {
    private String attack;
    private int height;
    private String ability;
    public void bark(){
        System.out.println("normal bark!"+"|||");
    }
    public NormalBark(){
        super();
    }

    public NormalBark clone(){
        Object obj = null;
        try {
            obj = super.clone();
            return (NormalBark)obj;
        }catch (CloneNotSupportedException e){
            System.out.println("不支持复制");
            return null;
        }
    }
    public NormalBark(String attack, int height, String ability) {
        this.attack = attack;
        this.height = height;
        this.ability = ability;
    }

    public String getAttack() {
        return attack;
    }

    public void setAttack(String attack) {
        this.attack = attack;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public String getAbility() {
        return ability;
    }

    public void setAbility(String ability) {
        this.ability = ability;
    }

    @Override
    public String toString() {
        return "NormalBark{" +
                "attack='" + attack + '\'' +
                ", height=" + height +
                ", ability='" + ability + '\'' +
                '}';
    }
}
