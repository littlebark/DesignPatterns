package com.bark.PrototypePattern;

import java.io.Serializable;

/**
 * Created by littlebark on 2017/9/29.
 */
public class Attachment implements Serializable {
    private String attack;
    private String skill;

    public Attachment() {
    }

    public Attachment(String attack, String skill) {
        this.attack = attack;
        this.skill = skill;
    }

    @Override
    public String toString() {
        return "Attachment{" +
                "attack='" + attack + '\'' +
                ", skill='" + skill + '\'' +
                '}';
    }

    public String getAttack() {
        return attack;
    }

    public void setAttack(String attack) {
        this.attack = attack;
    }

    public String getSkill() {
        return skill;
    }

    public void setSkill(String skill) {
        this.skill = skill;
    }
}
