package com.bark.PrototypePattern;

/**
 * Created by littlebark on 2017/9/29.
 */
public class CloneA implements Cloneable {
    private String foot;
    private String hand;

    public CloneA() {
    }

    public CloneA clone(){
        Object obj = null;
        try {
            obj = (CloneA)super.clone();
            return (CloneA) obj;
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
            return null;
        }
    }
    public CloneA(String foot, String hand) {
        this.foot = foot;
        this.hand = hand;
    }

    public String getFoot() {
        return foot;
    }

    public void setFoot(String foot) {
        this.foot = foot;
    }

    public String getHand() {
        return hand;
    }

    public void setHand(String hand) {
        this.hand = hand;
    }

    @Override
    public String toString() {
        return "CloneA{" +
                "foot='" + foot + '\'' +
                ", hand='" + hand + '\'' +
                '}';
    }
}
