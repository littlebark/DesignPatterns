package com.bark.PrototypePattern;

/**
 * Created by littlebark on 2017/9/28.
 */
public class Test {
    public static void main(String[] args) {
        /*NormalBark bark = new NormalBark("attack",100,"jump");
        NormalBark bark2 = bark.clone();
        bark.setAttack("aaaa");
        System.out.println(bark.toString());
        System.out.println(bark2.toString());*/
        CloneA a = new CloneA("foota","handa");
        CloneB b = new CloneB();
        b.setCloneA(a);
        b.setEar("ear");
        b.setHead("head");
        CloneB c = b.clone();
        //由于是潜克隆，对于bc保存一样的cloneA的地址，b对clonea的成员变量做了操作，c的clonea的成员变量是一样的（同一个clonea
        b.getCloneA().setFoot("footb");
        //在经过把所有地址克隆的属性重新克隆后，b和c就分开了
        System.out.println(b.toString()+"|||||"+c.toString());
    }
}
