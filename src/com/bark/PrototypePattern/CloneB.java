package com.bark.PrototypePattern;

/**
 * Created by littlebark on 2017/9/29.
 */
public class CloneB implements Cloneable {
    private String head;
    private String ear;
    private CloneA cloneA;

    public CloneB() {
    }

    public CloneB clone(){
        Object obj = null;
        try {
            obj =  super.clone();
            CloneB b = (CloneB)obj;
            //地址克隆的属性cloneA要重新克隆一次(新分配内存
            b.cloneA = this.cloneA.clone();
            return b;
        } catch (CloneNotSupportedException e) {
            System.out.println("不支持克隆");
            return null;
        }
    }

    @Override
    public String toString() {
        return "CloneB{" +
                "head='" + head + '\'' +
                ", ear='" + ear + '\'' +
                ", cloneA=" + cloneA +
                '}';
    }

    public String getHead() {
        return head;
    }

    public void setHead(String head) {
        this.head = head;
    }

    public CloneA getCloneA() {
        return cloneA;
    }

    public void setCloneA(CloneA cloneA) {
        this.cloneA = cloneA;
    }

    public String getEar() {
        return ear;
    }

    public void setEar(String ear) {
        this.ear = ear;
    }
}
